# -*- coding: utf-8 -*-

import json
import requests

from flask import Flask, request
app = Flask(__name__)


def get_product_info(pid):
    url = 'http://product-v3-americanas-npf.internal.b2w.io/product/{pid}'
    response = requests.get(url.format(pid=pid))
    if response.ok:
        return response.json()
    else:
        raise RuntimeError('Erro na obtenção do produto: ' + pid)


def get_offer_info(oid):
    url = 'http://offer-v1-americanas-npf.internal.b2w.io/offer/{oid}'
    response = requests.get(url.format(oid=oid))
    if response.ok:
        return response.json()
    else:
        raise RuntimeError('Erro na obtenção da offer: ' + oid)


def calculate_products_metrics(products):
    metrics = {}

    best_rated = {}
    best_rating = best_rated.get('rating', 0)

#get_product_name_and_id(products)

    for product in products:
        productId = product['id']
        product_data = get_product_info(productId)
        product_info = {
            'name': product_data['name'],
            'rating': product_data['rating']
        }

        metrics[product_data['id']] = product_info

#get_highest_rating_product(products)

    for product in products:

        best_rated = metrics.get('best_rated', {})
        best_rating = best_rated.get('rating', 0)

        if best_rating < product_data['rating']['average']:
            best_rated['pid'] = product_data['id']
            best_rated['rating'] = product_data['rating']['average']
            metrics['best_rated'] = best_rated

    return metrics


@app.route('/product/<pid>')
def product(pid):
    return get_product_info(pid)


@app.route('/products/metrics', methods=['GET', 'POST'])
def products_metrics():
    data = request.get_json()
    return calculate_products_metrics(data['products'])


@app.route('/offer/<oid>')
def offer(oid):
    return get_offer_info(oid)


if __name__ == '__main__':
    app.run(debug=True)
