# -*- coding: utf-8 -*-

import pytest
from unittest import mock
import app


app.get_product_info = mock.Mock(side_effect=[{
    'id': 1,
    'name': 'Iphone 1',
    'rating': {
        'average': 1
    }
}, {
    'id': 2,
    'name': 'Iphone 2',
    'rating': {
        'average': 2
    }
}])


@pytest.fixture
def products():
    return [{
        'id': 1
    }, {
        'id': 2
    }]


class TestProductsMetric:
    def test_metrics_format(self, products):
        metrics = app.calculate_products_metrics(products)
        assert 'best_rated' in metrics
        assert 'rating' in metrics['best_rated']
        assert 'pid' in metrics['best_rated']

        best_rated = metrics['best_rated']
        assert best_rated['rating'] == 2
        assert best_rated['pid'] == 2
